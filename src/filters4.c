/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filters4.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lpilotto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/31 12:13:27 by lpilotto          #+#    #+#             */
/*   Updated: 2016/10/31 12:15:46 by lpilotto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void		crt_effect(t_mlx *mlx)
{
	pixelize(mlx, 3);
	color_shift(mlx, 1);
	brightness(mlx, 50);
	scanline(mlx, 1.1f, 3);
	barrel_distortion(mlx, 0.2f, 1.8f);
}

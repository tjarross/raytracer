printf "Commit message : "
read msg
make fclean
if [ ! -z $1 ]
then
	for var in "$@"
	do
		git add $var
	done
else
	git add --all
fi

git commit -m "$msg"
git push

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rot.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pcrosnie <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/04 17:30:42 by pcrosnie          #+#    #+#             */
/*   Updated: 2016/10/19 16:37:32 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

cl_float3	rot(cl_float3 v, double anglex, double angley, double anglez)
{	
	float tmpx = v.x;
	float tmpy = v.y;
	float cosx = cos(anglex);
	float cosy = cos(angley);
	float cosz = cos(anglez);
	float sinx = sin(anglex);
	float siny = sin(angley);
	float sinz = sin(anglez);
	v.y = (v.y * cosx) - (v.z * sinx);
	v.z = (tmpy * sinx) + (v.z * cosx);
	v.x = (v.x * cosy) + (v.z * siny);
	v.z = (tmpx * -siny) + (v.z * cosy);
	tmpx = v.x;
	v.x = (v.x * cosz) - (v.y * sinz);
	v.y = (tmpx * sinz) + (v.y * cosz);
	return (v);
}

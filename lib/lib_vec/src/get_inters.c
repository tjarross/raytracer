/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_inters.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/29 17:34:58 by tjarross          #+#    #+#             */
/*   Updated: 2016/09/08 13:50:11 by pcrosnie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

cl_float3	get_inters(cl_float3 origin, cl_float3 raydir, double t)
{
	return (add_vec(origin, multiply_vec(raydir, t)));
}

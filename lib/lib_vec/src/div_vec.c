/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   div_vec.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/29 17:33:29 by tjarross          #+#    #+#             */
/*   Updated: 2016/09/04 15:57:09 by pcrosnie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

cl_float3	div_vec(cl_float3 v1, double n)
{
	v1.x /= n;
	v1.y /= n;
	v1.z /= n;
	return (v1);
}

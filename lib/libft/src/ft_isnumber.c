/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isnumber.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/01 01:55:31 by tjarross          #+#    #+#             */
/*   Updated: 2016/11/01 01:55:31 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	ft_isnumber(const char *str)
{
	while (ft_isspace(*str))
		str++;
	if (*str == '-' || *str == '+')
		str++;
	while (*str >= '0' && *str <= '9')
		str++;
	if (*str == '-' || *str == '+')
		return (0);
	if (*str == '.')
		str++;
	while (*str >= '0' && *str <= '9')
		str++;
	while (ft_isspace(*str))
		str++;
	return (*str == 0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/29 23:31:10 by tjarross          #+#    #+#             */
/*   Updated: 2016/02/29 23:31:10 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_memcmp(const void *s1, const void *s2, size_t n)
{
	unsigned const char	*c1;
	unsigned const char	*c2;

	c1 = (unsigned const char *)s1;
	c2 = (unsigned const char *)s2;
	while (n--)
	{
		if (*c1++ != *c2++)
			return (*--c1 - *--c2);
	}
	return (0);
}
